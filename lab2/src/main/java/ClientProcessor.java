import java.io.*;
import java.net.Socket;

public class ClientProcessor implements Runnable {

    private Socket socket;

    public ClientProcessor(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread() + " thread ran for new client on port " + socket.getPort());
        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {

            String message = retrieve(inputStream);
            System.out.println("MESSAGE! " + message);
            send("gotcha! \n" + message, outputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String retrieve(InputStream inputStream) throws IOException {
        InputStreamReader reader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
        while (bufferedReader.ready()) {
            builder.append((char) bufferedReader.read());
        }
        return builder.toString();
    }

    private void send(String message, OutputStream outputStream) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(message);
        bufferedWriter.flush();
    }
}
