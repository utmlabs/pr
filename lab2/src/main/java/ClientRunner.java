import java.io.IOException;
import java.net.Socket;

public class ClientRunner {

    public static void main(String[] args) throws IOException, InterruptedException {
        Client client = new Client();
        String host = "localhost";
        Integer port = 27032;
        Socket socket = new Socket(host, port);
        client.send("Message 1 abcdefghijklmnopqrstuvwxyzABCDEFG", socket.getOutputStream());
        Thread.sleep(1000);
        System.out.println("Retrieved: " + client.retrieve(socket.getInputStream()));
    }
}
