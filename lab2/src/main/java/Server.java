import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class Server {

    private Integer port;

    private ExecutorService executorService;

    private ThreadFactory clientProcessorThreadFactory;

    public Server() {
        this.clientProcessorThreadFactory = new ClientProcessorThreadFactory();
        this.executorService = Executors.newCachedThreadPool(clientProcessorThreadFactory);
    }

    public Server(Integer port) {
        this.port = port;
        this.clientProcessorThreadFactory = new ClientProcessorThreadFactory();
        this.executorService = Executors.newCachedThreadPool(clientProcessorThreadFactory);
    }

    public void run() throws IOException {
        ServerSocket server = startServer();
        while (!Thread.currentThread().isInterrupted()) {
            Socket client = acceptConnection(server);
            Runnable clientProcessor = new ClientProcessor(client);
            executorService.submit(clientProcessor);
        }

    }

    private ServerSocket startServer() throws IOException {
        if (port != null) {
            return new ServerSocket(port);
        }
        return new ServerSocket();
    }

    private synchronized Socket acceptConnection(ServerSocket serverSocket) throws IOException {
        return serverSocket.accept();
    }
}
