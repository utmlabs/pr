import java.io.*;

public class Client {

    public String retrieve(InputStream inputStream) throws IOException {
        InputStreamReader reader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
//        char[] buffer = new char[1024];
        while (bufferedReader.ready()) {
            builder.append(bufferedReader.readLine());
//            bufferedReader.read(buffer);
//            builder.append(buffer);
        }
        return builder.toString();
    }

    public void send(String message, OutputStream outputStream) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(message);
        bufferedWriter.flush();
    }
}
