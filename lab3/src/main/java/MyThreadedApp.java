import java.util.Arrays;

public class MyThreadedApp {


    public static void main(String[] args) throws Throwable {
        MyThread t1 = new MyThread("t1");
        MyThread t2 = new MyThread("t2");
        MyThread t3 = new MyThread("t3").runAfter(Arrays.asList(t1, t2));
        MyThread t4 = new MyThread("t4").runAfter(t3);
        MyThread t5 = new MyThread("t5").runAfter(t4);
        MyThread t6 = new MyThread("t6").runAfter(t5);

        System.out.println("FIRST RUN");
        t1.start();
        t3.start();
        t4.start();
        t2.start();
        t5.start();
        t6.start();

        Thread.sleep(1000);
        //must use new instances - once terminated, a Thread cannot be resurrected
        MyThread th1 = new MyThread("t1");
        MyThread th2 = new MyThread("t2");
        MyThread th3 = new MyThread("t3").runAfter(Arrays.asList(th1, th2));
        MyThread th4 = new MyThread("t4").runAfter(th3);
        MyThread th5 = new MyThread("t5").runAfter(th4);
        MyThread th6 = new MyThread("t6").runAfter(th5);

        System.out.println("\nSECOND RUN");
        th3.start();
        th4.start();
        th1.start();
        th5.start();
        th2.start();
        th6.start();

    }
}
