import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MyThread extends Thread {

    private ArrayList<Thread> dependantOn = new ArrayList<>();

    MyThread(final String name) {
        super(name);
    }


    @Override
    public synchronized void start() {
        super.start();
    }

    @Override
    public void run() {
        for (Thread t : dependantOn) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(String.format("[%s]: Thread %s Started", LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSSS")), getName()));
        super.run();
        System.out.println(String.format("[%s]: Thread %s Stopped", LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSSS")), getName()));
    }

    MyThread runAfter(Thread thread) {
        dependantOn.add(thread);
        return this;
    }

    MyThread runAfter(List<Thread> threads) {
        dependantOn.addAll(threads);
        return this;
    }
}
