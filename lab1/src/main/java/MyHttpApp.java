import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class MyHttpApp {

    private static URI getRequestUri = URI.create("https://httpbin.org/get");

    private static URI postRequestUri = URI.create("https://httpbin.org/post");


    public static void main(String[] args) throws IOException, InterruptedException {
        sendRequestsThroughJava11();
        sendRequestsThroughJava8();

    }

    private static void sendRequestsThroughJava11() throws IOException, InterruptedException {
        HttpResponse.BodyHandler<String> bodyHandler = HttpResponse.BodyHandlers.ofString();
        HttpRequest getRequest = HttpRequest.newBuilder(getRequestUri).GET().build();
        HttpClient httpClient = HttpClient.newHttpClient();

        String getBody = httpClient.send(getRequest, bodyHandler).body();
        System.out.println("GET REQUEST with Java 11:");
        System.out.println(getBody);

        HttpRequest.BodyPublisher publisher = HttpRequest.BodyPublishers.ofString("This is the message I am gonna post! :)");
        HttpRequest postRequest = HttpRequest.newBuilder(postRequestUri).POST(publisher).build();
        String postBody = httpClient.send(postRequest, bodyHandler).body();

        System.out.println("POST REQUEST with Java 11:");
        System.out.println(postBody);

    }

    private static void sendRequestsThroughJava8() throws IOException {
        HttpURLConnection getConnection = (HttpURLConnection) getRequestUri.toURL().openConnection();
        getConnection.setRequestMethod("GET");
        InputStream getResponseStream = getConnection.getInputStream();
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getResponseStream));
        bufferedReader.lines().forEach(stringBuilder::append);

        System.out.println("GET REQUEST with Java 8:");
        System.out.println(stringBuilder.toString());


        HttpURLConnection postConnection = (HttpURLConnection) postRequestUri.toURL().openConnection();
        postConnection.setRequestMethod("POST");
        InputStream postResponseStream = postConnection.getInputStream();
        stringBuilder = new StringBuilder();
        bufferedReader = new BufferedReader(new InputStreamReader(postResponseStream));
        bufferedReader.lines().forEach(stringBuilder::append);

        System.out.println("POST REQUEST with Java 11:");
        System.out.println(stringBuilder.toString());


    }
}
